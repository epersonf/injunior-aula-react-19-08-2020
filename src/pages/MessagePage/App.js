import React from 'react';
import './MessagePage.css';
import Chat from '../../components/ChatComponents/Chat/Chat';
import InputComponent from '../../components/InputComponents/InputComponent';
import axios from 'axios';


export const APIContext = React.createContext();

//Declaracao de API
let api = 
axios.create(
  {
      baseURL: 'https://treinamentoajax.herokuapp.com/'
  }
);

function App() {

    return (
        <section className="message-section">
          <APIContext.Provider value={api}>
            <InputComponent />
            <Chat />
          </APIContext.Provider>
        </section>
    );
}

export default App;
