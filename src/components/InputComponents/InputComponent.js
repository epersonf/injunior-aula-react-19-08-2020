import React, {useContext, useState} from 'react';
import './InputComponent.css';
import {APIContext} from '../../pages/MessagePage/App';

function InputComponent(props) {
    const api = useContext(APIContext);

    const [title, setTitle] = useState('');
    const [message, setMessage] = useState('');

    //#region Enviar mensagem
    const clickButton = (e) => {

        if (title === '' || message === '') {
            alert("Voce precisa preencher os campos para enviar uma mensagem.");
            return;
        }
        
        api.post('messages', 
            {
                "message": {
                    "name": title,
                    "message": message
                }
            }
          )
          .then(function (response) {
            console.log(response);
          })
          .catch(function (error) {
            console.log(error);
        });
    }
    //#endregion

    return (
        <section className='input-component'>
            <h4>Nome:</h4>
            <input type="text" onChange={(e) => setTitle(e.target.value)}></input>
            <h4>Mensagem:</h4>
            <textarea type="text" onChange={(e) => setMessage(e.target.value)}></textarea>
            <button  onClick={clickButton}>Enviar</button>
        </section>
    );
}

export default InputComponent;
