import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import * as serviceWorker from './serviceWorker';
import Header from './components/PageComponents/Header/Header'
import Footer from './components/PageComponents/Footer/Footer'
import { Route } from "react-router";
import { BrowserRouter } from 'react-router-dom';

import App from './pages/MessagePage/App';
import HomePage from './pages/HomePage/HomePage';

ReactDOM.render(
  <React.StrictMode>
    <Header />
    
    <BrowserRouter>
      <Route exact path = '/feed'>
        <App />
      </Route>
      <Route exact path = '/'>
        <HomePage />
      </Route>
    </BrowserRouter>

    <Footer />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
